class RemoveAccessLevelFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :access_level, :string
  end
end
