class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.datetime :time
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :bookings, [:user_id, :created_at]
  end
end