class AddAccessLevelColumn < ActiveRecord::Migration
  def change
    add_column :users, :access_level, :string
    change_column_default :users, :access_level, "basic"
  end
end
