class Booking < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  validates :time, presence: true, uniqueness: true
end
