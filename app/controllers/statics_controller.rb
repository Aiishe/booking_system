class StaticsController < ApplicationController
  def home
    redirect_to current_user if logged_in?
  end
  
  def bookings
  end
  
  def contact
  end
end
