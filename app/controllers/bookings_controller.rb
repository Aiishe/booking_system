class BookingsController < ApplicationController
    before_action :logged_in_user, only: [:new, :create, :destroy]
    before_action :correct_user,   only: :destroy
    
    def new
        @booking = Booking.new
        @user = current_user
    end
    
    def create
      @booking = current_user.bookings.build(booking_params)
      if @booking.save
        flash[:success] = "Booking created!"
        redirect_to root_url
      else
        flash[:danger] = "That timeslot has already been booked. Please select another timeslot."
        render 'statics/bookings'
      end
    end
    
    def destroy
      @booking.destroy
      flash[:success] = "Booking deleted!"
      redirect_to request.referrer || root_url
    end
    
    private

    def booking_params
      params.require(:booking).permit(:time)
    end
    
    def correct_user
      @booking = current_user.bookings.find_by(id: params[:id])
      redirect_to root_url if @booking.nil?
    end
end