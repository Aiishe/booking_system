class ApplicationMailer < ActionMailer::Base
  default from: "squash@stpauls.school.nz"
  layout 'mailer'
end
