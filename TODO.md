
Successful creation of new user currently reroutes back to homepage. Be sure to
change this to email validation page.

The application currently uses Heroku's SSL certificate. When moving the
application onto a school server, talk to Mr. Ward about using the school's
SSL certificate.

Remember to reset the database before moving onto the school server.

Adding admin accounts to database (through rails console):
user = User.new(name: "name", email: "email", password: "password",
                access_level: "admin")
user.save(options={validate: false})