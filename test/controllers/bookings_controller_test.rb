require 'test_helper'

class BookingsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @booking = bookings(:one)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Booking.count' do
      post bookings_path, booking: { time: 3.days.ago }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Booking.count' do
      delete booking_path(@booking)
    end
    assert_redirected_to login_url
  end
end