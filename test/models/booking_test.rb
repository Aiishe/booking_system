require 'test_helper'

class BookingTest < ActiveSupport::TestCase

  def setup
    @user = users(:nathan)
    # This code is not idiomatically correct.
    @booking = @user.bookings.build(time: Time.zone.now)
  end

  test "should be valid" do
    assert @booking.valid?
  end

  test "user id should be present" do
    @booking.user_id = nil
    assert_not @booking.valid?
  end
  
  test "booking.time should not be nil" do
    @booking.time = nil
    assert_not @booking.valid?
  end
end