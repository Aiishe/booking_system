require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:nathan)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    patch user_path(@user), user: { name:  "",
                                              password:              "foo",
                                              password_confirmation: "bar" }

  end
  
  test "successful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    name  = "Nathan Cleaver"
    patch user_path(@user), user: { name:  name,
                                              password:              "",
                                              password_confirmation: "" }
    assert_not flash.empty?
    #assert_redirected_to user_path
    @user.reload
    assert_equal name,  @user.name
  end
  
  
end