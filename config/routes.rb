Rails.application.routes.draw do
  root 'statics#home'
  get    '/signup',       to: 'users#new'
  get    '/login',        to: 'sessions#new'
  get    '/bookings',     to: 'statics#bookings'
  get    '/contact',      to: 'statics#contact'
  post   '/login',        to: 'sessions#create'
  delete '/logout',       to: 'sessions#destroy'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :bookings,            only: [:create, :destroy, :new]
end